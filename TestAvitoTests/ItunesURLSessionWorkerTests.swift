//
//  ItunesURLSessionWorkerTests.swift
//  TestAvito
//
//  Created by Artem Peskishev on 12.02.17.
//  Copyright © 2017 Artem Peskishev. All rights reserved.
//

import XCTest
@testable import TestAvito

class ItunesURLSessionWorkerTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testItunesURLSessionWorkerGetData() {
        let worker = ItunesURLSessionWorker(urlSessionConfiguration: URLSessionConfiguration.default)
        
        let expectation = self.expectation(description: "Load complete")
        
        worker.getDataWith(searchString: "asd") { (items, error) in
            
            if let items = items {
                XCTAssert(items.count>0, "Items count shoud be greatest than 0")
            } else {
                XCTAssert(false, "Items should not be nil")
            }
            expectation.fulfill()
        }
        
        self.waitForExpectations(timeout: 15, handler: nil)
    }
    
}
