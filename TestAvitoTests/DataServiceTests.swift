//
//  DataServiceTests.swift
//  TestAvito
//
//  Created by Artem Peskishev on 12.02.17.
//  Copyright © 2017 Artem Peskishev. All rights reserved.
//

import XCTest
@testable import TestAvito

class DataServiceTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    func testGetData() {
        let dataService = DataService(networkWorker: ItunesURLSessionWorker(urlSessionConfiguration: URLSessionConfiguration.default), mapper: ItunesSimpleMapper())
        
        let expectation = self.expectation(description: "Load complete")
        
        dataService.getDataWith(searchString: "asd") { (items, error) in
            if let items = items {
                XCTAssert(items.count>0, "Items count shoud be greatest than 0")
            } else {
                XCTAssert(false, "Items should not be nil")
            }
            expectation.fulfill()
        }
        
        self.waitForExpectations(timeout: 15, handler: nil)
    }
    
}
