//
//  GithubSimpleMapperTests.swift
//  TestAvito
//
//  Created by Artem Peskishev on 12.02.17.
//  Copyright © 2017 Artem Peskishev. All rights reserved.
//

import XCTest
@testable import TestAvito

class GithubSimpleMapperTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    func testMap() {
        let arrayOfDicts = [["avatar_url" : "link", "html_url" : "address", "login" : "userName"]]
        
        let mapper = GithubSimpleMapper()
        
        let searchObjects = mapper.map(dictArray: arrayOfDicts)
        
        XCTAssertEqual(searchObjects.count, 1)
        XCTAssertEqual(searchObjects.first?.author, "address")
        XCTAssertEqual(searchObjects.first?.title, "userName")
        XCTAssertEqual(searchObjects.first?.imageURL, "link")
    }

}
