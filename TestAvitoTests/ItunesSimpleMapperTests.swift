//
//  ItunesSimpleMapperTests.swift
//  TestAvito
//
//  Created by Artem Peskishev on 12.02.17.
//  Copyright © 2017 Artem Peskishev. All rights reserved.
//

import XCTest
@testable import TestAvito

class ItunesSimpleMapperTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    func testMap() {
        let arrayOfDicts = [["artworkUrl100" : "link", "artistName" : "artist", "trackName" : "track"]]
        
        let mapper = ItunesSimpleMapper()
        
        let searchObjects = mapper.map(dictArray: arrayOfDicts)
        
        XCTAssertEqual(searchObjects.count, 1)
        XCTAssertEqual(searchObjects.first?.author, "artist")
        XCTAssertEqual(searchObjects.first?.title, "track")
        XCTAssertEqual(searchObjects.first?.imageURL, "link")
    }
    
}
