//
//  SearchInteractorTests.swift
//  TestAvito
//
//  Created by Artem Peskishev on 12.02.17.
//  Copyright © 2017 Artem Peskishev. All rights reserved.
//

import XCTest
@testable import TestAvito

class SearchInteractorTests: XCTestCase {
    
    let searchInteractor = SearchInteractor(sourceType: SourceType.itunes, factory: ConcreteServiceFactory())
    
    override func setUp() {
        super.setUp()
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    func testGetDataWithEmptyString() {
        let interactorOutputMock = InteractorOutputMock()
        searchInteractor.interactorOutput = interactorOutputMock
        
        searchInteractor.getDataWith(searchString: "")
        
        XCTAssert(interactorOutputMock.beginUpdateWasCalled, "begin update should be called")
        XCTAssertEqual(interactorOutputMock.messageToShow, StringConstants.noQuery)
    }
    
    func testGetDataWithSomeString() {
        let interactorOutputMock = InteractorOutputMock()
        
        searchInteractor.interactorOutput = interactorOutputMock
        let loadCompleteExpectation = self.expectation(description: "Load complete")

        interactorOutputMock.onUpdateWasCalled = {
            loadCompleteExpectation.fulfill()
        }
        
        searchInteractor.getDataWith(searchString: "asd")
        
        XCTAssert(interactorOutputMock.beginUpdateWasCalled, "begin update should be called")
        
        self.waitForExpectations(timeout: 15, handler: nil)
    }
}

class InteractorOutputMock: SearchInteractorOutput {
    var messageToShow = ""
    var beginUpdateWasCalled = false
    
    var onUpdateWasCalled: (() -> Void)?
    
    func beginUpdate() { beginUpdateWasCalled = true }
    func updateData(_ data : [SearchObject]) { onUpdateWasCalled?() }
    func showError(_ error: String) { messageToShow = error }
}
