//
//  TestAvitoTests.swift
//  TestAvitoTests
//
//  Created by Artem Peskishev on 09.02.17.
//  Copyright © 2017 Artem Peskishev. All rights reserved.
//

import XCTest
@testable import TestAvito

class GitURLSessionWorkerTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    func testGitURLSessionWorkerGetData() {
        let worker = GitURLSessionWorker(urlSessionConfiguration: URLSessionConfiguration.default)
        
        let expectation = self.expectation(description: "Load complete")
        
        worker.getDataWith(searchString: "asd") { (items, error) in

            if let items = items {
                XCTAssert(items.count>0, "Items count shoud be greatest than 0")
            } else {
                XCTAssert(false, "Items should not be nil")
            }
            expectation.fulfill()
        }
        
        self.waitForExpectations(timeout: 15, handler: nil)
    }
}
