//
//  ItunesSimpleMapper.swift
//  TestAvito
//
//  Created by Artem Peskishev on 10.02.17.
//  Copyright © 2017 Artem Peskishev. All rights reserved.
//

import Foundation

final class ItunesSimpleMapper: Mapper {
    func map(dictArray: [[String : Any]]) -> [SearchObject] {
        
        let objectsArray = dictArray.map { objectDict -> ItunesSearchResult in
            var itunesStruct = ItunesSearchResult(imageURL: "", title: "", author: "")
            
            if let artworkURL = objectDict["artworkUrl100"] as? String {
                itunesStruct.imageURL = artworkURL
            }
            
            if let artistName = objectDict["artistName"] as? String {
                itunesStruct.author = artistName
            }
            
            if let trackName = objectDict["trackName"] as? String {
                itunesStruct.title = trackName
            }
            
            return itunesStruct
        }
        
        return objectsArray
    }
}
