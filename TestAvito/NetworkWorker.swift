//
//  NetworkWorker.swift
//  TestAvito
//
//  Created by Artem Peskishev on 09.02.17.
//  Copyright © 2017 Artem Peskishev. All rights reserved.
//

import Foundation

protocol NetworkWorker {
    func getDataWith(searchString: String, completion: @escaping (([Any]?, Error?) -> Void))
    func cancelTask()
}
