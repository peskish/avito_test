//
//  LeftAlignmentCell.swift
//  TestAvito
//
//  Created by Artem Peskishev on 09.02.17.
//  Copyright © 2017 Artem Peskishev. All rights reserved.
//

import UIKit

protocol SearchObjectCellDelegate : class {
    func showImageFrom(_ indexPath: IndexPath)
}

final class SearchObjectCell: UITableViewCell {

    @IBOutlet weak var iconImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var authorLabel: UILabel!
    
    var indexPath: IndexPath?
    
    weak var delegate: SearchObjectCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        nameLabel.text = ""
        authorLabel.text = ""
        
        iconImageView.isUserInteractionEnabled = true
        let imageTap = UITapGestureRecognizer(target: self, action: #selector(showImage))
        iconImageView.addGestureRecognizer(imageTap)
    }
    
    func configureCellWith(object: SearchObject) {
        nameLabel.text = object.title
        authorLabel.text = object.author
        iconImageView.setImageWith(urlString: object.imageURL)
    }
    
    func showImage() {
        guard let indexPath = indexPath else { return }
        delegate?.showImageFrom(indexPath)
    }
}
