//
//  URLSessionWorker.swift
//  TestAvito
//
//  Created by Artem Peskishev on 09.02.17.
//  Copyright © 2017 Artem Peskishev. All rights reserved.
//

import Foundation

class URLSessionWorker {
    
    let session : URLSession
    var dataTask: URLSessionDataTask?

    init(urlSessionConfiguration: URLSessionConfiguration) {
        urlSessionConfiguration.allowsCellularAccess = true
        urlSessionConfiguration.httpAdditionalHeaders = ["Accept": "application/json"]
        self.session = URLSession(configuration: urlSessionConfiguration)
    }
    
    func cancelTask() {
        if let dataTask = dataTask {
            dataTask.cancel()
        }
    }
}
