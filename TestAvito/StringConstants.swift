//
//  StringConstants.swift
//  TestAvito
//
//  Created by Artem Peskishev on 12.02.17.
//  Copyright © 2017 Artem Peskishev. All rights reserved.
//

import Foundation

struct StringConstants {
    static let unknownError = "Что то пошло не так. Попробуйте снова."
    static let noData = "По данному запросу ничего не найдено"
    static let noQuery = "Введите что-нибудь в строку поиска"
    static let searching = "Идет поиск..."
}
