//
//  GithubSearchResult.swift
//  TestAvito
//
//  Created by Artem Peskishev on 09.02.17.
//  Copyright © 2017 Artem Peskishev. All rights reserved.
//

import Foundation

struct GithubSearchResult: SearchObject {
    var imageURL: String
    var title: String
    var author: String
}
