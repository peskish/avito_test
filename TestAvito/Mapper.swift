//
//  Mapper.swift
//  TestAvito
//
//  Created by Artem Peskishev on 09.02.17.
//  Copyright © 2017 Artem Peskishev. All rights reserved.
//

import Foundation

protocol Mapper {
    func map(dictArray: [[String : Any]]) -> [SearchObject]
}
