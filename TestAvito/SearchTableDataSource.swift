//
//  SearchTableDataSource.swift
//  TestAvito
//
//  Created by Artem Peskishev on 10.02.17.
//  Copyright © 2017 Artem Peskishev. All rights reserved.
//

import Foundation
import UIKit

protocol SearchTableDataSourceDelegate: class {
    func showImageFrom(_ searchObject: SearchObject, indexPath: IndexPath)
}

enum CellMode {
    case leftAlignmentFirst, rightAlignmentFirst
}

final class SearchTableDataSource: NSObject, UITableViewDataSource {
 
    var searchObjects = [SearchObject]()
    var cellMode : CellMode = .rightAlignmentFirst
    weak var delegate: SearchTableDataSourceDelegate?
    
    func reuseIdentifierFor(indexPath:IndexPath) -> String {
        let isEven = (indexPath.row+1) % 2 == 1
        if (isEven && cellMode == .leftAlignmentFirst) || (!isEven && cellMode == .rightAlignmentFirst) {
            return "LeftAlignmentCell"
        } else {
            return "RightAlignmentCell"
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return searchObjects.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: reuseIdentifierFor(indexPath: indexPath)) as! SearchObjectCell
        cell.configureCellWith(object: searchObjects[indexPath.row])
        cell.delegate = self
        cell.indexPath = indexPath
        return cell
    }
    
    func getRectForImageView(tableView: UITableView, indexPath: IndexPath) -> CGRect {
        let cell = tableView.cellForRow(at: indexPath) as! SearchObjectCell
        let rectOfCellInTableView = tableView.rectForRow(at: indexPath)
        let rectOfCellInSuperview = tableView.convert(rectOfCellInTableView, to: tableView.superview)

        let rectOfImageViewInCell = CGRect(x: CGFloat(cell.iconImageView.frame.origin.x),
                                           y: CGFloat(cell.iconImageView.frame.origin.y + rectOfCellInSuperview.origin.y),
                                           width: CGFloat(cell.iconImageView.frame.size.width),
                                           height: CGFloat(cell.iconImageView.frame.size.height))
        
        return rectOfImageViewInCell
    }
}

extension SearchTableDataSource: SearchObjectCellDelegate {
    func showImageFrom(_ indexPath: IndexPath) {
        delegate?.showImageFrom(searchObjects[indexPath.row], indexPath: indexPath)
    }
}
