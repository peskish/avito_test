//
//  ItunesURLSessionWorker.swift
//  TestAvito
//
//  Created by Artem Peskishev on 09.02.17.
//  Copyright © 2017 Artem Peskishev. All rights reserved.
//

import Foundation

fileprivate let API_ITUNES_SEARCH_URL = "https://itunes.apple.com/search?"

final class ItunesURLSessionWorker: URLSessionWorker, NetworkWorker {
    func getDataWith(searchString: String, completion: @escaping (([Any]?, Error?) -> Void)) {
        let searchURL = API_ITUNES_SEARCH_URL + "term=\(searchString)"
        if let url = URL(string: searchURL.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!) {
            
            cancelTask()
            
            dataTask = session.dataTask(with: url) {
                data, response, error in

                if let error = error {
                    if error.localizedDescription != "cancelled" {
                        DispatchQueue.main.async {
                            completion(nil, error)
                        }
                    }
                } else if let httpResponse = response as? HTTPURLResponse {
                    if let data = data, httpResponse.statusCode == 200 {
                        let resultDict = try? JSONSerialization.jsonObject(with: data, options: []) as? [AnyHashable : Any]
                        let resultArray = resultDict??["results"]
                        DispatchQueue.main.async {
                            completion(resultArray as? [Any], nil)
                        }
                    } else {
                        DispatchQueue.main.async {
                            completion(nil, UnknownError.customError)
                        }
                    }
                } else {
                    DispatchQueue.main.async {
                        completion(nil, UnknownError.customError)
                    }
                }
            }
            dataTask?.resume()
        }
    }
}
