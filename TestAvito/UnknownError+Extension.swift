//
//  UnknownError+Extension.swift
//  TestAvito
//
//  Created by Artem Peskishev on 11.02.17.
//  Copyright © 2017 Artem Peskishev. All rights reserved.
//

import Foundation

extension UnknownError: LocalizedError {
    public var errorDescription: String? {
        switch self {
        case .customError:
            return StringConstants.unknownError
        }
    }
}
