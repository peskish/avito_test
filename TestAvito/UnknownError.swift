//
//  UnknownError.swift
//  TestAvito
//
//  Created by Artem Peskishev on 10.02.17.
//  Copyright © 2017 Artem Peskishev. All rights reserved.
//

import Foundation

enum UnknownError: Error {
    case customError
}


