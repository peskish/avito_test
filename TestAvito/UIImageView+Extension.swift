//
//  UIImageView+Extension.swift
//  TestAvito
//
//  Created by Artem Peskishev on 10.02.17.
//  Copyright © 2017 Artem Peskishev. All rights reserved.
//

import Foundation
import UIKit

fileprivate let imageCache = NSCache<AnyObject, AnyObject>()

extension UIImageView {
    func setImageWith(urlString: String) {
        self.image = #imageLiteral(resourceName: "placeholder")
        
        if let cachedImage = imageCache.object(forKey: urlString as AnyObject) as? UIImage {
            self.image = cachedImage
            return
        }
        
        guard let url = URL(string: urlString) else {return}
        
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            
            guard error == nil, let data = data, let downloadedImage = UIImage(data: data)
                else { return }
            
            DispatchQueue.main.async {
                imageCache.setObject(downloadedImage, forKey: urlString as AnyObject)
                self.image = downloadedImage
            }
        }.resume()
}
}
