//
//  AppDelegate.swift
//  TestAvito
//
//  Created by Artem Peskishev on 09.02.17.
//  Copyright © 2017 Artem Peskishev. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var interactor: SearchInteractor?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
       
        if let window = window,
            let navViewController = window.rootViewController as? UINavigationController,
            let viewController = navViewController.topViewController as? SearchViewController {
            
            let sourceTypes = [SourceType.itunes, .github]
            
            interactor = SearchInteractor(sourceType:sourceTypes[0], factory: ConcreteServiceFactory())
            interactor?.interactorOutput = viewController
            
            viewController.dataSource = SearchTableDataSource()
            viewController.eventHandler = interactor
            viewController.sourceTypes = sourceTypes
        }

        return true
    }
}

