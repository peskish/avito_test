//
//  SearchViewController.swift
//  TestAvito
//
//  Created by Artem Peskishev on 09.02.17.
//  Copyright © 2017 Artem Peskishev. All rights reserved.
//

import UIKit

final class SearchViewController: UIViewController {

    weak var eventHandler: SearchInteractor?
    var searchString = ""
    var dataSource : SearchTableDataSource?
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var infoLabel: UILabel!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    @IBOutlet var overlayView: UIView!
    @IBOutlet var largeImageView: UIImageView!
    var oldFrame = CGRect.zero

    @IBOutlet weak var segmentedControl: UISegmentedControl!
    var sourceTypes = [SourceType]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.dataSource = dataSource
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 58
        
        dataSource?.delegate = self
        
        let imageTap = UITapGestureRecognizer(target: self, action: #selector(self.hideImage))
        largeImageView.addGestureRecognizer(imageTap)
        
        setupSegmentedControl()
        
        updateInfoLabelWith(text: StringConstants.noQuery)
    }
    
    @IBAction func segmentedControlValueChanged(_ sender: UISegmentedControl) {
        switch sender.selectedSegmentIndex {
        case 0:
            dataSource?.cellMode = .rightAlignmentFirst
        default:
            dataSource?.cellMode = .leftAlignmentFirst
        }
        
        eventHandler?.changeSource(type: sourceTypes[sender.selectedSegmentIndex])
        eventHandler?.getDataWith(searchString: searchString)

    }
    
    func updateInfoLabelWith(text: String) {
        tableView.isHidden = true
        infoLabel.text = text
    }
    
    func setupSegmentedControl() {
        segmentedControl.removeAllSegments()
        for (index, sourceType) in sourceTypes.enumerated() {
            segmentedControl.insertSegment(withTitle: sourceType.rawValue, at: index, animated: false)
        }
        segmentedControl.selectedSegmentIndex = 0
    }
}

extension SearchViewController: SearchInteractorOutput {
    
    func beginUpdate() {
        activityIndicator.startAnimating()
        updateInfoLabelWith(text: StringConstants.searching)
    }
    
    func updateData(_ data: [SearchObject]) {
        activityIndicator.stopAnimating()
        dataSource?.searchObjects = data
        tableView.isHidden = false
        tableView.reloadData()
    }
    
    func showError(_ error: String) {
        activityIndicator.stopAnimating()
        updateInfoLabelWith(text: error)
    }
}

extension SearchViewController: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        searchString = searchText
        eventHandler?.getDataWith(searchString: searchText)
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        view.endEditing(true)
    }
}

extension SearchViewController: SearchTableDataSourceDelegate {
    func showImageFrom(_ searchObject: SearchObject, indexPath: IndexPath) {
        view.endEditing(true)
        guard let dataSource = dataSource else { return}
        
        let searchObject = dataSource.searchObjects[indexPath.row]
        
        oldFrame = dataSource.getRectForImageView(tableView: tableView, indexPath: indexPath)
        largeImageView.setImageWith(urlString: searchObject.imageURL)
        largeImageView.frame = oldFrame
        
        if let currentWindow = UIApplication.shared.keyWindow {
            overlayView.alpha = 0
            overlayView.addSubview(largeImageView)
            overlayView.frame = currentWindow.bounds
            currentWindow.addSubview(overlayView)
            
            UIView.animate(withDuration: 0.28) {
                let frame = self.view.frame
                self.largeImageView.frame = frame
                self.overlayView.alpha = 1
            }
        }
    }
    
    func hideImage() {
        UIView.animate(withDuration: 0.28, animations: { 
            let frame = self.oldFrame
            self.largeImageView.frame = frame
            self.overlayView.alpha = 0
        }) { (success) in
            self.overlayView.removeFromSuperview()
        }
    }
}
