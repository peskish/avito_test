//
//  SearchObject.swift
//  TestAvito
//
//  Created by Artem Peskishev on 09.02.17.
//  Copyright © 2017 Artem Peskishev. All rights reserved.
//

import Foundation

protocol SearchObject {
    var imageURL: String { get set }
    var title: String { get set }
    var author: String { get set }
}
