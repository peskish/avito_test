//
//  DataSourceType.swift
//  TestAvito
//
//  Created by Artem Peskishev on 11.02.17.
//  Copyright © 2017 Artem Peskishev. All rights reserved.
//

import Foundation

enum SourceType: String {
    case itunes, github
}
