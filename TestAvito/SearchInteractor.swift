//
//  SearchInteractor.swift
//  TestAvito
//
//  Created by Artem Peskishev on 09.02.17.
//  Copyright © 2017 Artem Peskishev. All rights reserved.
//

import Foundation

protocol SearchInteractorOutput: class {
    func beginUpdate()
    func updateData(_ data : [SearchObject])
    func showError(_ error: String)
}

protocol SearchInteractorInput {
    func getDataWith(searchString: String?)
    func changeSource(type: SourceType)
}

final class SearchInteractor: SearchInteractorInput {
    
    var dataService: DataService
    var dataServiceFactory: DataServiceFactory
    var currentSourceType: SourceType
    weak var interactorOutput: SearchInteractorOutput?
    
    required init(sourceType: SourceType, factory: DataServiceFactory) {
        self.dataServiceFactory = factory
        self.currentSourceType = sourceType
        self.dataService = self.dataServiceFactory.createDataService(type: sourceType)
    }
    
    // MARK: SearchInteractorInput
    
    func getDataWith(searchString: String?) {
        interactorOutput?.beginUpdate()
        if let searchString = searchString, searchString.characters.count > 0 {
            dataService.getDataWith(searchString: searchString) { [weak self] (items, error) in
                if let error = error {
                    self?.interactorOutput?.showError(error.localizedDescription)
                } else if let items = items {
                    if items.count > 0 {
                        self?.interactorOutput?.updateData(items)
                    } else {
                        self?.interactorOutput?.showError(StringConstants.noData)
                    }
                } else {
                    self?.interactorOutput?.showError(StringConstants.unknownError)
                }
            }
        } else {
            self.dataService.cancelTasks()
            self.interactorOutput?.showError(StringConstants.noQuery)
        }
    }
    
    func changeSource(type : SourceType) {
        currentSourceType = type
        dataService = dataServiceFactory.createDataService(type: type)
    }
}
