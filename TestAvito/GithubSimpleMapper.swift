//
//  GithubSimpleMapper.swift
//  TestAvito
//
//  Created by Artem Peskishev on 10.02.17.
//  Copyright © 2017 Artem Peskishev. All rights reserved.
//

import Foundation

final class GithubSimpleMapper: Mapper {
    func map(dictArray: [[String : Any]]) -> [SearchObject] {
        
        let objectsArray = dictArray.map { objectDict -> GithubSearchResult in
            var gitHubStruct = GithubSearchResult(imageURL: "", title: "", author: "")
            if let imageURL = objectDict["avatar_url"] as? String {
                gitHubStruct.imageURL = imageURL
            }
            
            if let authorString = objectDict["html_url"] as? String {
                gitHubStruct.author = authorString
            }
            
            if let login = objectDict["login"] as? String {
                gitHubStruct.title = login
            }
            
            return gitHubStruct
        }
        
        return objectsArray
    }
}
