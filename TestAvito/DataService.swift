//
//  DataService.swift
//  TestAvito
//
//  Created by Artem Peskishev on 09.02.17.
//  Copyright © 2017 Artem Peskishev. All rights reserved.
//

import Foundation

final class DataService {
    var networkWorker: NetworkWorker
    var mapper: Mapper
    
    required init(networkWorker: NetworkWorker, mapper: Mapper) {
        self.networkWorker = networkWorker
        self.mapper = mapper
    }
    
    func getDataWith(searchString: String, completion: @escaping (([SearchObject]?, Error?) -> Void)) {
        networkWorker.getDataWith(searchString: searchString) { [weak self] (items, error) in
            if let items = items as? [[String : AnyObject]] {
                completion(self?.mapper.map(dictArray: items), nil)
            } else if let error = error {
                completion(nil, error)
            }
        }
    }
    
    func cancelTasks() {
        networkWorker.cancelTask()
    }
}
