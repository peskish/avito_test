//
//  DataServiceFabric.swift
//  TestAvito
//
//  Created by Artem Peskishev on 11.02.17.
//  Copyright © 2017 Artem Peskishev. All rights reserved.
//

import Foundation

protocol DataServiceFactory {
    func createDataService(type: SourceType) -> DataService
}

class ConcreteServiceFactory : DataServiceFactory {
    func createDataService(type: SourceType) -> DataService {
        switch type {
        case .itunes:
            return DataService(networkWorker: ItunesURLSessionWorker(urlSessionConfiguration: URLSessionConfiguration.default), mapper: ItunesSimpleMapper())
            
        case .github:
            return DataService(networkWorker: GitURLSessionWorker(urlSessionConfiguration: URLSessionConfiguration.default), mapper: GithubSimpleMapper())
        }
    }
}
